#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <wait.h>
#include <stdbool.h>
#include <pwd.h>
#include <grp.h>
#define PATH "/home/helsanesta/modul2/"

void createList(char *myPath, char *folderAir){
    DIR *dir;
    struct dirent *ep;
    FILE *fptr;
    int r;
    struct stat izin;
    char destination[100];
    strcpy(destination, myPath);
    strcat(destination, folderAir);
    strcat(destination, "/");
    char list[100];
    strcpy(list, destination);
    strcat(list, "list.txt");

    fptr = fopen(list, "w+");  //open both reading and writing, if doesnt exist, create it
    dir = opendir(destination);

    if(dir != NULL){
        while(ep = readdir(dir)){
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                char getpermission[100];
                strcpy(getpermission, destination);
                strcat(getpermission, ep->d_name);

                r = stat(getpermission, &izin);
                if(r== -1){
                    fprintf(stderr, "File error\n");
                    exit(1);
                }
                struct passwd *pw = getpwuid(izin.st_uid);

                char rwx[10];
                strcpy(rwx, "");
                if( izin.st_mode & S_IRUSR )
                    strcat(rwx, "r");
                if( izin.st_mode & S_IWUSR )
                    strcat(rwx, "w");
                if( izin.st_mode & S_IXUSR )
                    strcat(rwx, "x");
                else
                    strcat(rwx, "-");
                // fprintf(fptr, "%s\n", rwx);
                char nameAnimal[100];
                strcpy(nameAnimal, pw->pw_name);
                strcat(nameAnimal, "_");
                strcat(nameAnimal, rwx);
                strcat(nameAnimal, "_");
                strcat(nameAnimal, ep->d_name);
                fprintf(fptr, "%s\n", nameAnimal);
            }
        } (void) closedir(dir);
    }
    else{
        perror("Couldn't open the directory");
    }
}

void deleteBird(char *myPath, char *folderDarat){
    pid_t child_id = fork();
    int status;

    if(child_id==0){
        char sourceFile[100];
        strcpy(sourceFile, myPath);
        strcat(sourceFile, folderDarat);
        strcat(sourceFile, "/");


        char *argv[] = {"find", sourceFile, "-name", "*bird*", "-delete", NULL};
        execv("/bin/find", argv);
    }else{
        while((wait(&status)) > 0);
    }
}

void deleteAnimal(char *myPath, char *folderAnimal){
    pid_t child_id = fork();
    int status;

    if(child_id==0){
        char sourceFile[100];
        strcpy(sourceFile, myPath);
        strcat(sourceFile, folderAnimal);
        strcat(sourceFile, "/");

        char *argv[] = {"rm", "-r", sourceFile, NULL};
        execv("/bin/rm", argv);
    }else{
        while((wait(&status)) > 0);
    }
}


void move(char *folderAnimal, char *myPath, char *folderDest){
    pid_t child_id = fork();
    int status;

    if(child_id== 0){
        DIR *dir;
        struct dirent *ep;
        char category[100];
        strcpy(category, folderDest);

        char sourceFile[100];
        //char curDir[100];
        strcpy(sourceFile, myPath);
        strcat(sourceFile, folderAnimal);
        strcat(sourceFile, "/");

        dir = opendir(sourceFile);

        if(dir != NULL){
            char desFile[100];
            strcpy(desFile, myPath);
            strcat(desFile, folderDest);
            strcat(desFile, "/");

            while(ep = readdir(dir)){
                char fileAnimal[100];
                strcpy(fileAnimal, sourceFile);
                strcat(fileAnimal, ep->d_name);
                if(strstr(ep->d_name,category) != NULL){
                    pid_t child_id2 = fork();
                    if(child_id2==0){
                        char *argv[]={"mv", fileAnimal, desFile, NULL};
                        execv("/bin/mv", argv);
                    }else{
                        wait(0);
                    }
                }

            }
        }else{
            while((wait(&status)) > 0);
        }
    }
}

void unzip(char *myPath)
{
  pid_t child_id;
  int status;
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  } if (child_id == 0)  {
    char curPath[100];
    strcpy(curPath, myPath);
    char *argv[] = {"unzip", "-d", curPath,"animal.zip",NULL};
    execv("/bin/unzip", argv);
  }
  else{
    wait(0);
  }
}

void makedir(char *myPath, char *key)
{
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id == 0)  {
        char curPath[100];
        strcpy(curPath, myPath);
        strcat(curPath, key);
        char *argv[] = {"mkdir", "-p", curPath, NULL};
        execv("/bin/mkdir", argv);
    }
}

int main(){
    pid_t child_id;
    int status;
    int cek=0;

    child_id = fork();
    
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        makedir(PATH, "darat");
    } else {
        while((wait(&status)) > 0);
        sleep(3);
        makedir(PATH, "air");
        wait(0);
        sleep(1);
        unzip(PATH);
        wait(0);
        cek++;
    }
    if(cek==1){
        pid_t child_id2 = fork();
        int status2;

        if(child_id2==0){
            move("animal", PATH, "darat");
            wait(0);
            sleep(1);
            move("animal", PATH, "air");
            wait(0);
        }else{
            while((wait(&status)) > 0);
            deleteAnimal(PATH, "animal");
            wait(0);
            cek++;
        }
    }
    if(cek==2){
        pid_t child_id3 = fork();
       // int status3;

        if(child_id3 == 0){
            deleteBird(PATH, "darat");
            wait(0);
        }
        else{
            cek++;
        }
    }
    if(cek==3){
        pid_t child_id4 = fork();

        if(child_id4 ==0){
            createList(PATH, "air");
            wait(0);
        }
    }
}