#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <wait.h>
#include <stdbool.h>
#define PATH "/home/helsanesta/shift2/drakor/"

void delete_substr(char *filename){     // https://stackoverflow.com/questions/43163677/how-do-i-strip-a-file-extension-from-a-string-in-c
    char *end = filename + strlen(filename);

    while(end >filename && *end != '.'){
        --end;
    }

    if (end > filename){
        *end = '\0';
    }
}

void listingdir(char *myPath)
{
  DIR *dp;
  struct dirent *ep;
  char path[PATH_MAX];

  strcpy(path, myPath);

  dp = opendir(path);

  if (dp != NULL)
  {
    while ((ep = readdir (dp))) {
        puts (ep->d_name);
    }

    (void) closedir (dp);
  } else perror ("Couldn't open the directory");
}

bool validate (char *filename, char *key){
  if(strstr(filename, key) != NULL)
  {
    return true;
  }
  return false;
}

void movecontent(char *myPath, char *foldergenre, char *key)
{
  pid_t child_id = fork();
  if(child_id ==0){
    DIR *dp;
    struct  dirent *ep;
    char folderPath[PATH_MAX];
    strcpy(folderPath, myPath);

    dp = opendir(folderPath);
    if(dp != NULL){
      char folderDest[PATH_MAX];
      strcpy(folderDest, myPath);
      strcat(folderDest, foldergenre);
      strcat(folderDest, "/");

      while(ep = readdir(dp)){
        char filePath[PATH_MAX];
        strcpy(filePath,folderPath);
        strcat(filePath,ep->d_name);
        if(validate(ep->d_name, key)){
          pid_t child_id2 = fork();
          if(child_id2==0){
            char *argv[] = {"cp", filePath, folderDest, NULL};
            execv("/bin/cp", argv);
          }else{
            wait(0);
          }
        }
      }
    }else{
      int status;
      while((wait(&status)) > 0);
    }
  }
}

void deldir(char *myPath)
{
  pid_t child_id;
  int status;
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  } if (child_id == 0)  {
    char curPath[100];
    strcpy(curPath, myPath);
    char *argv[] = {"rm", "-r", "/home/helsanesta/shift2/drakor/*.png",NULL};
    execv("/bin/rm", argv);
  }
  else{
    wait(0);
  }
}

void makesub(char *myPath)
{
  char My[PATH_MAX];
  char path[PATH_MAX];
  DIR *dir;
  struct dirent *dp;
  strcpy(My, myPath);
  pid_t child_id;
  int status;
  dir = opendir(myPath);

  while ((dp = readdir(dir)) != NULL)
  {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            // Construct new path from our base path
            strcpy(path, My);
            strcat(path, "/");
            strcat(path, dp->d_name);
            if(child_id<0)
            	exit(EXIT_FAILURE);
            if(child_id==0)
            {
              //wait(0);
            	char getnamefile[PATH_MAX];
            	strcpy(getnamefile, dp->d_name);

            	delete_substr(getnamefile);
            	
            	char name[PATH_MAX];
              char *split;
              split = strtok(getnamefile, ";");

              int arr = 0;
              while(split != NULL){
                  if(arr == 2){
                    strcpy(name, split);
                    if(strlen(name) <= 8){
                        strcpy(My, myPath);
                        strcat(My, name);
                        // printf("%s\n", name);
                          
                        char *argv[] = {"mkdir", "-p", My, NULL};
                        execv("/bin/mkdir", argv);
                    }
                  }
                  split = strtok(NULL, ";");
                  arr++;
                }
            }
            wait(0);
            child_id=fork();
        }
  }      
}


void unzip(char *myPath)
{
  pid_t child_id;
  int status;
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  } if (child_id == 0)  {
    char curPath[100];
    strcpy(curPath, myPath);
    char *argv[] = {"unzip", "-d", curPath,"drakor.zip", "*.png",NULL};
    execv("/bin/unzip", argv);
  }
  else{
    wait(0);
  }
}
void makedir(char *myPath)
{
  pid_t child_id;
  int status;
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  } if (child_id == 0)  {
    char curPath[100];
    strcpy(curPath, myPath);
    char *argv[] = {"mkdir", "-p", curPath, NULL};
    execv("/bin/mkdir", argv);
  }
}

int main() {
  pid_t child_id;
  int status;
  //int cek=0;

  child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    makedir(PATH);
  } else {
    wait(0);
    sleep(1);
    unzip(PATH);
    sleep(2);
    makesub(PATH);
    wait(0);
   
  }

  // pid_t child_id2 = fork();
  // if(child_id2 == 0){
  //   movecontent(PATH, "action", "action.png");
  //   wait(0);
  // }
  // pid_t child_id3 = fork();
  // if(child_id3 == 0){
  //   movecontent(PATH, "comedy", "comedy.png");
  //   wait(0);
  // }
  // pid_t child_id4 = fork();
  // if(child_id4 == 0){
  //   movecontent(PATH, "fantasy", "fantasy.png");
  //   wait(0);
  // }
  // pid_t child_id5 = fork();
  // if(child_id5 == 0){
  //   movecontent(PATH, "horror", "horror.png");
  //   wait(0);
  // }
  // pid_t child_id6 = fork();
  // if(child_id6 == 0){
  //   movecontent(PATH, "romance", "romance.png");
  //   wait(0);
  // }
  // pid_t child_id7 = fork();
  // if(child_id7 == 0){
  //   movecontent(PATH, "school", "school.png");
  //   wait(0);
  // }
  // pid_t child_id8 = fork();
  // if(child_id8 == 0){
  //   movecontent(PATH, "thriller", "thriller.png");
  //   wait(0);
  // }
  // if(cek == 1){
  //   pid_t child_id2 = fork();

  //   if (child_id2 < 0)
  //   {
  //     exit(EXIT_FAILURE);
  //   }
  //   else{
  //     wait(0);
  //     sleep(2);
  //     movecontent(PATH);
  //   }
  // }
}
