# soal-shift-sisop-modul-2-B09-2022 

## Anggota Kelompok
| **Nama** | **NRP** |
| ------ | ------ |
| Helsa Nesta Dhaifullah | 5025201005 |
| Zunia Aswaroh | 5025201058 |
| Tengku Fredly Reinaldo | 5025201198 |

# Soal 3


### Poin A
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

1. Define Path untuk Directory awal 
`#define PATH "/home/helsanesta/modul2/"`

2. Dalam function main(), buat program dengan fork, dan jika child_id = 0, mkdir darat di dalam DIrectory awal
```
int main(){
    pid_t child_id;
    int status;
    int cek=0;

    child_id = fork();
    
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        makedir(PATH, "darat");
    } 
```        
3. Function makedir(PATH, "darat")
```
void makedir(char *myPath, char *key)
{
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id == 0)  {
        char curPath[100];
        strcpy(curPath, myPath);
        strcat(curPath, key);
        char *argv[] = {"mkdir", "-p", curPath, NULL};
        execv("/bin/mkdir", argv);
    }
}
```


> -  fork untuk spawning procces.
> -  strcpy untuk copy PATH kemudian strcat untuk menempelkan key (darat/air).
> -  exec adalah fungsi untuk menjalankan program baru dan menggantikan program yang sedang berjalan.

4. Function makedir (PATH, "air") 

Lanjutan dari function main()
```
else {
        while((wait(&status)) > 0);
        sleep(3);
        makedir(PATH, "air");
        wait(0);
        sleep(1);
        unzip(PATH);
        wait(0);
        cek++;
    }
```
> -  wait untuk delay suatu proses.
> -  sleep(3) untuk set waktu dijalankannya suatu proses.

### Poin B
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

1. Extract menggunakan function unzip(PATH)
```
void unzip(char *myPath)
{
  pid_t child_id;
  int status;
  child_id = fork();
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  } if (child_id == 0)  {
    char curPath[100];
    strcpy(curPath, myPath);
    char *argv[] = {"unzip", "-d", curPath,"animal.zip",NULL};
    execv("/bin/unzip", argv);
  }
  else{
    wait(0);
  }
}
```
> - To unzip a ZIP file to a different directory than the current one, use the unzip -d
> - Unzip ke Directory awal /home/[USER]/modul2/

### Poin C
hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

1. Move semua foto hewan dengan filename "darat" / "air" dari folder Animal ke folder darat / folder air

> /home/[USER]/modul2/animal/ -> /home/[USER]/modul2/darat/

> /home/[USER]/modul2/animal/ -> /home/[USER]/modul2/air/
```
    if(cek==1){
        pid_t child_id2 = fork();
        int status2;

        if(child_id2==0){
            move("animal", PATH, "darat");
            wait(0);
            sleep(1);
            move("animal", PATH, "air");
            wait(0);
        }else{
            while((wait(&status)) > 0);
            deleteAnimal(PATH, "animal");
            wait(0);
            cek++;
        }
    }
```
Function move("animal", PATH, "darat" / "air")
```
void move(char *folderAnimal, char *myPath, char *folderDest){
    pid_t child_id = fork();
    int status;

    if(child_id== 0){
        DIR *dir;
        struct dirent *ep;
        char category[100];
        strcpy(category, folderDest);

        char sourceFile[100];
        //char curDir[100];
        strcpy(sourceFile, myPath);
        strcat(sourceFile, folderAnimal);
        strcat(sourceFile, "/");

        dir = opendir(sourceFile);

        if(dir != NULL){
            char desFile[100];
            strcpy(desFile, myPath);
            strcat(desFile, folderDest);
            strcat(desFile, "/");

            while(ep = readdir(dir)){
                char fileAnimal[100];
                strcpy(fileAnimal, sourceFile);
                strcat(fileAnimal, ep->d_name);
                if(strstr(ep->d_name,category) != NULL){
                    pid_t child_id2 = fork();
                    if(child_id2==0){
                        char *argv[]={"mv", fileAnimal, desFile, NULL};
                        execv("/bin/mv", argv);
                    }else{
                        wait(0);
                    }
                }

            }
        }else{
            while((wait(&status)) > 0);
        }
    }
}
```

> - set sorceFile ke path folder animal.
> - Jika tidak NULL, set desFile ke path folder darat / air.
> - readdir folder animal, kemudian jika ep->d_name mengandung kata sama dengan category (darat/air), maka execv move.

2. Delete gambar yang tidak ada keterangan darat/air beserta folder Animal
```
    if(cek==1){
        pid_t child_id2 = fork();
        int status2;

        if(child_id2==0){
            move("animal", PATH, "darat");
            wait(0);
            sleep(1);
            move("animal", PATH, "air");
            wait(0);
        }else{
            while((wait(&status)) > 0);
            deleteAnimal(PATH, "animal");
            wait(0);
            cek++;
        }
    }
```
Function deleteAnimal(PATH, "animal")
```
void deleteAnimal(char *myPath, char *folderAnimal){
    pid_t child_id = fork();
    int status;

    if(child_id==0){
        char sourceFile[100];
        strcpy(sourceFile, myPath);
        strcat(sourceFile, folderAnimal);
        strcat(sourceFile, "/");

        char *argv[] = {"rm", "-r", sourceFile, NULL};
        execv("/bin/rm", argv);
    }else{
        while((wait(&status)) > 0);
    }
}
```
> - With -r option rm command performs a tree-walk and will delete all the files and sub-directories recursively of the parent directory. 

### Poin D
Menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

1. Menghapus bird di folder darat
```
 if(cek==2){
        pid_t child_id3 = fork();
       // int status3;

        if(child_id3 == 0){
            deleteBird(PATH, "darat");
            wait(0);
        }
        else{
            cek++;
        }
    }
```
Function deleteBird(PATH, "darat")
```
void deleteBird(char *myPath, char *folderDarat){
    pid_t child_id = fork();
    int status;

    if(child_id==0){
        char sourceFile[100];
        strcpy(sourceFile, myPath);
        strcat(sourceFile, folderDarat);
        strcat(sourceFile, "/");


        char *argv[] = {"find", sourceFile, "-name", "*bird*", "-delete", NULL};
        execv("/bin/find", argv);
    }else{
        while((wait(&status)) > 0);
    }
}
```
> - find dari sourcefile, yang mengandung filename *bird*, hapus.

### Poin E
Membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

1. Buat list.txt dalam folder air yang berisi nama semua hewan di folder air dengan format UID_filepermission_NamaFile
```
 if(cek==3){
        pid_t child_id4 = fork();

        if(child_id4 ==0){
            createList(PATH, "air");
            wait(0);
        }
    }
```
Function createList(PATH, "air")
```
void createList(char *myPath, char *folderAir){
    DIR *dir;
    struct dirent *ep;
    FILE *fptr;
    int r;
    struct stat izin;
    char destination[100];
    strcpy(destination, myPath);
    strcat(destination, folderAir);
    strcat(destination, "/");
    char list[100];
    strcpy(list, destination);
    strcat(list, "list.txt");

    fptr = fopen(list, "w+");  //open both reading and writing, if doesnt exist, create it
    dir = opendir(destination);

    if(dir != NULL){
        while(ep = readdir(dir)){
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                char getpermission[100];
                strcpy(getpermission, destination);
                strcat(getpermission, ep->d_name);

                r = stat(getpermission, &izin);
                if(r== -1){
                    fprintf(stderr, "File error\n");
                    exit(1);
                }
                struct passwd *pw = getpwuid(izin.st_uid);

                char rwx[10];
                strcpy(rwx, "");
                if( izin.st_mode & S_IRUSR )
                    strcat(rwx, "r");
                if( izin.st_mode & S_IWUSR )
                    strcat(rwx, "w");
                if( izin.st_mode & S_IXUSR )
                    strcat(rwx, "x");
                else
                    strcat(rwx, "-");
                // fprintf(fptr, "%s\n", rwx);
                char nameAnimal[100];
                strcpy(nameAnimal, pw->pw_name);
                strcat(nameAnimal, "_");
                strcat(nameAnimal, rwx);
                strcat(nameAnimal, "_");
                strcat(nameAnimal, ep->d_name);
                fprintf(fptr, "%s\n", nameAnimal);
            }
        } (void) closedir(dir);
    }
    else{
        perror("Couldn't open the directory");
    }
}
```
> - Referensi Source Code -> [UID Permission](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2#extras)
> - create FILE list.txt dari Source di -> [C-File-Input-Output](https://www.programiz.com/c-programming/c-file-input-output)
> - Melihat Permission dari suatu file atau directory di bahasa C dengan library yang bernama sys/stat.h.
> - S_IRUSR untuk Read, S_IWUSR untuk write, S_IXUSR untuk execute.
> - Setelah itu print string yang sudah sesuai format ke dalam file list.txt.

### Kendala
Awalnya saya bingung mengenai UID, tetapi setelah melihat modul kembali, ternyata sudah ada source code yang bisa digunakan. Kendala lain adalah pada saat membuat fungsi untuk menghapus folder animal menggunakan rm, ketika di terminal, command rm bisa berjalan dengan baik, tetapi jika dimasukkan ke dalam execv, tidak bisa berjalan, namun sebagai gantinya saya menggunakan fungsi find kemudian delete.
